package com.datpham.golfzon.presentation.view.loadmore

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

const val SEARCH_START_PAGE = 1

open class RecyclerViewLoadMoreScroll(
    private val layoutManager: LinearLayoutManager,
    private val onLoadMore: ((page: Int) -> Unit)
) : RecyclerView.OnScrollListener() {

    private var visibleThreshold = 5
    private var isLoading: Boolean = false
    private var lastVisibleItem: Int = 0
    private var totalItemCount: Int = 0
    private var currentPage = SEARCH_START_PAGE

    fun refresh() {
        currentPage = SEARCH_START_PAGE
    }

    fun setLoaded() {
        isLoading = false
    }

    fun getLoaded(): Boolean {
        return isLoading
    }

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        if (dy <= 0) return

        totalItemCount = layoutManager.itemCount

        lastVisibleItem = layoutManager.findLastVisibleItemPosition()

        if (!isLoading && totalItemCount <= lastVisibleItem + visibleThreshold) {
            currentPage++
            onLoadMore(currentPage)
            isLoading = true
        }
    }
}