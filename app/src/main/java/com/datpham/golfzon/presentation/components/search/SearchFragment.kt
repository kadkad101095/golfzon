package com.datpham.golfzon.presentation.components.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.datpham.domain.common.AppError
import com.datpham.domain.entity.Photo
import com.datpham.golfzon.R
import com.datpham.golfzon.databinding.FragmentSearchBinding
import com.datpham.golfzon.presentation.base.BaseFragment
import com.datpham.golfzon.presentation.ext.MIN_KEYWORD_LENGTH
import com.datpham.golfzon.presentation.ext.debounceAfterTextChanged
import com.datpham.golfzon.presentation.ext.getErrorMessage
import com.datpham.golfzon.presentation.view.loadmore.RecyclerViewLoadMoreScroll
import com.datpham.golfzon.presentation.view.loadmore.SEARCH_START_PAGE
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SearchFragment : BaseFragment() {

    private lateinit var binding: FragmentSearchBinding
    private val viewModel: SearchViewModel by activityViewModels()
    private val searchAdapter = SearchAdapter()
    private lateinit var recyclerViewLoadMoreScroll: RecyclerViewLoadMoreScroll

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSearchBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListener()
        initView()
    }

    private fun initListener() {

        viewModel.state.observe(viewLifecycleOwner) {
            handleStateChange(it)
        }

        viewModel.searchListPhoto.observe(viewLifecycleOwner) {
            handleSearchListPhotos(it)
        }

        viewModel.selectedPhoto.observe(viewLifecycleOwner) {
            handleSelectedPhoto(it)
        }

    }

    private fun initView() {
        with(binding) {
            val layoutManager = LinearLayoutManager(context)
            recyclerViewLoadMoreScroll = RecyclerViewLoadMoreScroll(layoutManager) { page ->
                searchAdapter.setRefresh(false)
                viewModel.search(edtSearch.text.toString(), page)
            }

            rcvSearch.layoutManager = layoutManager
            rcvSearch.adapter = searchAdapter
            rcvSearch.addOnScrollListener(recyclerViewLoadMoreScroll)
            rcvSearch.setHasFixedSize(true)

            edtSearch.debounceAfterTextChanged(lifecycle = lifecycle) {
                if (it.length > MIN_KEYWORD_LENGTH) {
                    recyclerViewLoadMoreScroll.refresh()
                    searchAdapter.setRefresh(true)
                    viewModel.search(it, SEARCH_START_PAGE)
                }
            }

            searchAdapter.onItemClick = { position, listPhoto ->
                viewModel.updateDetailStatus(
                    selectedPhotoPosition = position,
                    currentListPhotos = listPhoto
                )
                findNavController().navigate(R.id.action_searchFragment_to_detailFragment)
            }
        }
    }

    private fun handleStateChange(state: SearchState) {
        when (state) {
            is SearchState.Loading -> {
                when (state.isLoading) {
                    true -> showLoading()
                    false -> hideLoading()
                }
            }
            is SearchState.Success -> {
                val newPhotos = state.photoResponse.photos
                recyclerViewLoadMoreScroll.setLoaded() //Notify scroll listener loaded, ready for next scroll
                searchAdapter.addData(newPhotos)
                if (newPhotos.isEmpty()) {
                    showErrorDialog(AppError.Empty.getErrorMessage(requireContext()))
                }
            }
            is SearchState.Failed -> {
                hideLoading()
                showErrorDialog(state.appError.getErrorMessage(requireContext()))
            }
        }
    }

    private fun handleSelectedPhoto(selectedPhotoPosition: Int) {
        (binding.rcvSearch.layoutManager as? LinearLayoutManager)?.scrollToPositionWithOffset(
            selectedPhotoPosition,
            0
        )
    }

    private fun handleSearchListPhotos(listPhotos: List<Photo>) {
        searchAdapter.setRefresh(true)
        searchAdapter.addData(listPhotos)
    }

}