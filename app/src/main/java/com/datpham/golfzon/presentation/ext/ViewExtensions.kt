package com.datpham.golfzon.presentation.ext

import androidx.appcompat.widget.AppCompatEditText
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.coroutineScope
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SnapHelper
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

const val DEBOUNCE_PERIOD: Long = 500
const val MIN_KEYWORD_LENGTH: Int = 2

fun AppCompatEditText.debounceAfterTextChanged(
    debouncePeriod: Long = DEBOUNCE_PERIOD,
    lifecycle: Lifecycle,
    onDebouncingQueryTextChange: (String) -> Unit
) {
    val coroutineScope = lifecycle.coroutineScope
    coroutineScope.launchWhenResumed {
        var searchJob: Job? = null
        this@debounceAfterTextChanged.doAfterTextChanged { editable ->
            searchJob?.cancel()
            searchJob = coroutineScope.launch {
                editable?.let {
                    delay(debouncePeriod)
                    onDebouncingQueryTextChange(editable.toString())
                }
            }
        }
    }
}

fun SnapHelper.getSnapPosition(recyclerView: RecyclerView): Int {
    val layoutManager = recyclerView.layoutManager ?: return RecyclerView.NO_POSITION
    val snapView = findSnapView(layoutManager) ?: return RecyclerView.NO_POSITION
    return layoutManager.getPosition(snapView)
}