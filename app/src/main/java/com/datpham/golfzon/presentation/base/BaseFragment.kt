package com.datpham.golfzon.presentation.base

import android.app.ProgressDialog
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.datpham.golfzon.R

abstract class BaseFragment : Fragment() {

    private val progressDialog: ProgressDialog by lazy { ProgressDialog(context) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progressDialog.setMessage(getString(R.string.loading))
        progressDialog.setCancelable(false)
    }

    fun showLoading() {
        progressDialog.show()
    }

    fun hideLoading() {
        progressDialog.cancel()
    }

    fun showErrorDialog(message: String) {
        AlertDialog.Builder(requireContext())
            .setTitle(R.string.error)
            .setMessage(message)
            .setPositiveButton(android.R.string.ok) { dialogInterface, _ ->
                dialogInterface.dismiss()
            }
            .show()
    }

    fun showConfirmDialog(message: String, onConfirmClick: (() -> Unit)) {
        val builder = AlertDialog.Builder(requireContext())
        builder.setMessage(message)
            .setCancelable(false)
            .setPositiveButton(getString(R.string.yes)) { dialog, id ->
                onConfirmClick()
            }
            .setNegativeButton(getString(R.string.no)) { dialog, id ->
                dialog.dismiss()
            }
        val alert = builder.create()
        alert.show()
    }

}