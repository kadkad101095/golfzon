package com.datpham.golfzon.presentation.components.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.datpham.domain.entity.Photo
import com.datpham.golfzon.databinding.ItemSearchBinding
import com.datpham.imageloader.ImageLoader

class SearchAdapter : RecyclerView.Adapter<SearchAdapter.SearchViewHolder>() {

    private val imageLoader = ImageLoader.instance
    private val resultList = arrayListOf<Photo>()
    private var isRefresh = true
    var onItemClick: ((position: Int, allPhotos: List<Photo>) -> Unit)? = null

    inner class SearchViewHolder(val binding: ItemSearchBinding) : ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchViewHolder {
        val binding = ItemSearchBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SearchViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        with(holder) {
            with(resultList[position]) {
                imageLoader.loadImage(binding.imageAvatar, this.src.small)
                binding.tvName.text = this.photographer
                binding.tvDescription.text = this.alt
                binding.background.setOnClickListener {
                    onItemClick?.invoke(position, resultList)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return resultList.size
    }

    //if isRefresh == true it will clear old data after get new data
    fun setRefresh(isRefresh: Boolean) {
        this.isRefresh = isRefresh
    }

    fun addData(newPhotos: List<Photo>) {
        val lastSize = resultList.size
        if (isRefresh) {
            resultList.clear()
            resultList.addAll(newPhotos)
            notifyItemRangeRemoved(0, lastSize)
            notifyItemRangeInserted(0, newPhotos.size)
        } else {
            resultList.addAll(newPhotos)
            notifyItemRangeInserted(lastSize, newPhotos.size)
        }
    }
}