package com.datpham.golfzon.presentation.components.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.datpham.domain.common.AppError
import com.datpham.domain.common.BaseResult
import com.datpham.domain.entity.Photo
import com.datpham.domain.entity.PhotoResponse
import com.datpham.domain.usecase.PhotoSearchUseCase
import com.datpham.golfzon.presentation.ext.getAppError
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

sealed class SearchState {
    data class Loading(val isLoading: Boolean) : SearchState()
    data class Success(val photoResponse: PhotoResponse) : SearchState()
    data class Failed(val appError: AppError) : SearchState()
}

const val DEFAULT_PAGE_SIZE = 15

@HiltViewModel
class SearchViewModel @Inject constructor(private val photoSearchUseCase: PhotoSearchUseCase) :
    ViewModel() {

    private val _state = MutableLiveData<SearchState>(SearchState.Loading(false))
    val state: LiveData<SearchState> get() = _state

    private val _detailListPhoto = MutableLiveData<List<Photo>>()
    val detailListPhoto: LiveData<List<Photo>> get() = _detailListPhoto

    private val _searchListPhoto = MutableLiveData<List<Photo>>()
    val searchListPhoto: LiveData<List<Photo>> get() = _searchListPhoto

    private val _selectedPhoto = MutableLiveData<Int>()
    val selectedPhoto: LiveData<Int> get() = _selectedPhoto

    fun search(
        query: String,
        page: Int,
        pageSize: Int = DEFAULT_PAGE_SIZE
    ) {
        viewModelScope.launch {
            photoSearchUseCase.execute(query, page, pageSize)
                .onStart {
                    _state.value = SearchState.Loading(true)
                }
                .catch { exception ->
                    _state.value = SearchState.Loading(false)
                    _state.value = SearchState.Failed(exception.getAppError())
                }
                .collect { baseResult ->
                    _state.value = SearchState.Loading(false)
                    when (baseResult) {
                        is BaseResult.Error -> {
                            _state.value = SearchState.Failed(baseResult.appError)
                        }
                        is BaseResult.Success -> {
                            _state.value = SearchState.Success(baseResult.data)
                        }
                    }
                }
        }
    }

    fun updateDetailStatus(selectedPhotoPosition: Int, currentListPhotos: List<Photo>) {
        _selectedPhoto.value = selectedPhotoPosition
        _detailListPhoto.value = currentListPhotos
    }

    fun updateSearchStatus(selectedPhotoPosition: Int, currentListPhotos: List<Photo>) {
        _selectedPhoto.value = selectedPhotoPosition
        _searchListPhoto.value = currentListPhotos
    }

}