package com.datpham.golfzon.presentation.components.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import com.datpham.domain.entity.Photo
import com.datpham.golfzon.R
import com.datpham.golfzon.databinding.FragmentDetailBinding
import com.datpham.golfzon.presentation.base.BaseFragment
import com.datpham.golfzon.presentation.components.search.SearchViewModel
import com.datpham.golfzon.presentation.ext.getSnapPosition


class DetailFragment : BaseFragment() {

    private lateinit var binding: FragmentDetailBinding
    private val viewModel: SearchViewModel by activityViewModels()
    private val detailAdapter = DetailAdapter()
    private val snapHelper = PagerSnapHelper()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListener()
        initView()
    }

    override fun onPause() {
        super.onPause()
        val position = snapHelper.getSnapPosition(binding.rcvDetail)
        //Update photo list to sync with search screen
        viewModel.updateSearchStatus(
            selectedPhotoPosition = position,
            currentListPhotos = detailAdapter.getPhotoList()
        )
    }

    private fun initListener() {

        viewModel.detailListPhoto.observe(viewLifecycleOwner) {
            handleDetailListPhotos(it)
        }

        viewModel.selectedPhoto.observe(viewLifecycleOwner) {
            handleSelectedPhoto(it)
        }

    }

    private fun initView() {
        with(binding) {
            val layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

            rcvDetail.layoutManager = layoutManager
            rcvDetail.adapter = detailAdapter
            rcvDetail.setHasFixedSize(true)

            snapHelper.attachToRecyclerView(rcvDetail)

            btnDelete.setOnClickListener {
                showConfirmDialog(getString(R.string.confirm_delete)) {
                    val position = snapHelper.getSnapPosition(rcvDetail)
                    detailAdapter.removeItem(position)
                }
            }
        }
    }

    private fun handleDetailListPhotos(listPhotos: List<Photo>) {
        detailAdapter.addData(listPhotos)
    }

    private fun handleSelectedPhoto(selectedPhotoPosition: Int) {
        (binding.rcvDetail.layoutManager as? LinearLayoutManager)?.scrollToPositionWithOffset(
            selectedPhotoPosition,
            0
        )
    }

}