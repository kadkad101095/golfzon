package com.datpham.golfzon.presentation.ext

import android.content.Context
import com.datpham.domain.common.*
import com.datpham.golfzon.R
import java.net.UnknownHostException

fun AppError.getErrorMessage(context: Context): String {
    val errorMapList = mapOf(
        Pair(ERROR_DEFAULT, context.getString(R.string.error_default)),
        Pair(ERROR_NO_INTERNET_CONNECTION, context.getString(R.string.error_no_internet)),
        Pair(ERROR_EMPTY_RESPONSE, context.getString(R.string.error_empty_response)),
        Pair(ERROR_BAD_REQUEST, context.getString(R.string.error_bad_request)),
        Pair(ERROR_SERVER_DOWN, context.getString(R.string.error_server_down))
    )
    return when (this) {
        is AppError.Error -> this.message
        else -> {
            errorMapList[this.code] ?: context.getString(R.string.error_default)
        }
    }
}

fun Throwable.getAppError(): AppError {
    return when (this) {
        is UnknownHostException -> AppError.NoInternet
        else -> AppError.Error(this.message ?: "Unknown Error")
    }
}