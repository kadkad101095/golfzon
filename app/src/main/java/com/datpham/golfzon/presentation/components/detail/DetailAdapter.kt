package com.datpham.golfzon.presentation.components.detail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.datpham.domain.entity.Photo
import com.datpham.golfzon.databinding.ItemDetailBinding
import com.datpham.imageloader.ImageLoader

class DetailAdapter : RecyclerView.Adapter<DetailAdapter.DetailViewHolder>() {

    private val imageLoader = ImageLoader.instance
    private val resultList = arrayListOf<Photo>()
    private var isRefresh = true

    inner class DetailViewHolder(val binding: ItemDetailBinding) : ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailViewHolder {
        val binding = ItemDetailBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return DetailViewHolder(binding)
    }

    override fun onBindViewHolder(holder: DetailViewHolder, position: Int) {
        with(holder) {
            with(resultList[position]) {
                imageLoader.loadImage(binding.imgDetail, this.src.large)
            }
        }
    }

    override fun getItemCount(): Int {
        return resultList.size
    }

    fun getPhotoList(): List<Photo> = resultList

    //if isRefresh == true it will clear old data after get new data
    fun setRefresh(isRefresh: Boolean) {
        this.isRefresh = isRefresh
    }

    fun addData(newPhotos: List<Photo>) {
        val lastSize = resultList.size
        if (isRefresh) {
            resultList.clear()
            resultList.addAll(newPhotos)
            notifyItemRangeRemoved(0, lastSize)
            notifyItemRangeInserted(0, newPhotos.size)
        } else {
            resultList.addAll(newPhotos)
            notifyItemRangeInserted(lastSize, newPhotos.size)
        }
    }

    fun removeItem(position: Int) {
        resultList.removeAt(position)
        notifyItemRemoved(position)
    }
}