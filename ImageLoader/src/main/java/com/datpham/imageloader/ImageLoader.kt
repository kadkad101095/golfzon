package com.datpham.imageloader

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Handler
import android.os.Looper
import android.widget.ImageView
import java.net.URL
import java.util.concurrent.Executors

class ImageLoader {

    companion object {
        val instance: ImageLoader by lazy(LazyThreadSafetyMode.SYNCHRONIZED) { ImageLoader() }
    }

    fun loadImage(imageView: ImageView, imageUrl: String?) {
        //TODO: Add cache handle for bitmap
        val executor = Executors.newSingleThreadExecutor()
        executor.execute {
            val handler = Handler(Looper.getMainLooper())
            val image: Bitmap?
            try {
                val inputStream = URL(imageUrl).openStream()
                image = BitmapFactory.decodeStream(inputStream)
                handler.post {
                    imageView.scaleType = ImageView.ScaleType.CENTER_CROP
                    imageView.setImageBitmap(image)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}