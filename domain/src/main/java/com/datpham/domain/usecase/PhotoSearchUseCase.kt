package com.datpham.domain.usecase

import com.datpham.domain.common.AppError
import com.datpham.domain.common.BaseResult
import com.datpham.domain.entity.PhotoResponse
import com.datpham.domain.repository.PhotoRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class PhotoSearchUseCase @Inject constructor(private val photoRepository: PhotoRepository) {
    suspend fun execute(
        query: String,
        page: Int,
        pageSize: Int
    ): Flow<BaseResult<PhotoResponse, AppError>> {
        return photoRepository.search(query, page, pageSize)
    }
}