package com.datpham.domain.common

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import com.datpham.domain.R
import retrofit2.Response
import java.net.HttpURLConnection

class AppErrorMapper(private val context: Context) {

    fun getAppError(response: Response<*>): AppError {
        return if (!isOnline(context)) {
            AppError.NoInternet
        } else {
            when (response.code()) {
                HttpURLConnection.HTTP_BAD_REQUEST -> AppError.BadRequest
                HttpURLConnection.HTTP_INTERNAL_ERROR -> AppError.ServerDown
                else -> AppError.Error(response.message())
            }
        }
    }

    private fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities =
            connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
        if (capabilities != null) {
            if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                return true
            } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                return true
            } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                return true
            }
        }
        return false
    }
}