package com.datpham.domain.common

sealed class AppError(val code: Int) {
    data class Error(val message: String) : AppError(ERROR_DEFAULT)
    object NoInternet : AppError(ERROR_NO_INTERNET_CONNECTION)
    object Empty : AppError(ERROR_EMPTY_RESPONSE)
    object BadRequest : AppError(ERROR_BAD_REQUEST)
    object ServerDown : AppError(ERROR_SERVER_DOWN)
}

const val ERROR_DEFAULT = -1
const val ERROR_NO_INTERNET_CONNECTION = -2
const val ERROR_EMPTY_RESPONSE = -3
const val ERROR_BAD_REQUEST = -4
const val ERROR_SERVER_DOWN = -5
