package com.datpham.domain.repository

import com.datpham.domain.common.AppError
import com.datpham.domain.common.BaseResult
import com.datpham.domain.entity.PhotoResponse
import kotlinx.coroutines.flow.Flow

interface PhotoRepository {
    suspend fun search(
        query: String,
        page: Int,
        pageSize: Int
    ): Flow<BaseResult<PhotoResponse, AppError>>
}