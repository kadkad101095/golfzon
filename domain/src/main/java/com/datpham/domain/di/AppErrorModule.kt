package com.datpham.domain.di

import android.content.Context
import com.datpham.domain.common.AppErrorMapper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppErrorModule {

    @Singleton
    @Provides
    fun provideAppErrorMapper(@ApplicationContext appContext: Context) : AppErrorMapper {
        return AppErrorMapper(appContext)
    }

}