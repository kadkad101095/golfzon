package com.datpham.domain.entity


data class PhotoResponse(
    var page: Int,
    var perPage: Int,
    var photos: List<Photo>,
)

data class Photo(
    val id: Int,
    val width: Int,
    val height: Int,
    val url: String,
    val photographer: String,
    val alt: String,
    val src: PhotoSrc
)

data class PhotoSrc(
    val original: String,
    val small: String,
    val medium: String,
    val large: String,
    val large2x: String,
    val portrait: String,
    val landscape: String
)