package com.datpham.data.remote.dto

import com.google.gson.annotations.SerializedName

data class DataPhotoResponse(
    @SerializedName("page") var page: Int,
    @SerializedName("per_page") var perPage: Int,
    @SerializedName("photos") var photos: List<DataPhoto>,
)

data class DataPhoto(
    val id: Int,
    val width: Int,
    val height: Int,
    val url: String,
    val photographer: String,
    val alt: String,
    val src: DataPhotoSrc
)

data class DataPhotoSrc(
    val original: String,
    val small: String,
    val medium: String,
    val large: String,
    val large2x: String,
    val portrait: String,
    val landscape: String
)
