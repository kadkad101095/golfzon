package com.datpham.data.remote.mapper

import com.datpham.data.remote.dto.DataPhotoResponse
import com.datpham.domain.entity.Photo
import com.datpham.domain.entity.PhotoResponse
import com.datpham.domain.entity.PhotoSrc

fun DataPhotoResponse.toPhotoResponse(): PhotoResponse {
    return PhotoResponse(
        page = page,
        perPage = perPage,
        photos = photos.map {
            Photo(
                id = it.id,
                width = it.width,
                height = it.height,
                url = it.url,
                photographer = it.photographer,
                alt = it.alt,
                src = PhotoSrc(
                    original = it.src.original,
                    small = it.src.small,
                    medium = it.src.medium,
                    large = it.src.large,
                    large2x = it.src.large2x,
                    portrait = it.src.portrait,
                    landscape = it.src.landscape
                ),
            )
        }
    )
}