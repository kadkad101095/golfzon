package com.datpham.data.di

import com.datpham.data.remote.api.PhotoApi
import com.datpham.data.repository.PhotoRepositoryImpl
import com.datpham.domain.common.AppErrorMapper
import com.datpham.domain.repository.PhotoRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class PhotoModule {

    @Singleton
    @Provides
    fun provideLoginApi(retrofit: Retrofit): PhotoApi {
        return retrofit.create(PhotoApi::class.java)
    }

    @Singleton
    @Provides
    fun provideLoginRepository(
        photoApi: PhotoApi,
        appErrorMapper: AppErrorMapper
    ): PhotoRepository {
        return PhotoRepositoryImpl(photoApi, appErrorMapper)
    }

}