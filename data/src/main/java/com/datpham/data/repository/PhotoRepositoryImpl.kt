package com.datpham.data.repository

import com.datpham.data.remote.api.PhotoApi
import com.datpham.data.remote.mapper.toPhotoResponse
import com.datpham.domain.common.AppError
import com.datpham.domain.common.AppErrorMapper
import com.datpham.domain.common.BaseResult
import com.datpham.domain.entity.PhotoResponse
import com.datpham.domain.repository.PhotoRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class PhotoRepositoryImpl (
    private val photoApi: PhotoApi,
    private val appErrorMapper: AppErrorMapper
) : PhotoRepository {

    override suspend fun search(
        query: String,
        page: Int,
        pageSize: Int
    ): Flow<BaseResult<PhotoResponse, AppError>> {
        return flow {
            val response = photoApi.search(
                query = query,
                page = page,
                perPage = pageSize
            )
            if (response.isSuccessful) {
                response.body()?.let {
                    val photoResponse = it.toPhotoResponse()
                    emit(BaseResult.Success(photoResponse))
                } ?: emit(BaseResult.Error(AppError.Empty))
            } else {
                emit(BaseResult.Error(appErrorMapper.getAppError(response)))
            }
        }
    }

}